import { createMuiTheme } from '@material-ui/core/styles';

export const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#FFE600'
        },
        secondary: {
            main: '#3483FA'
        },
    },
})