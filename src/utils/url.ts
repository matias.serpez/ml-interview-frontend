import _get from 'lodash/get';

// TODO: This could be to much easier.
export function getJsonSearch(search: string, searchKey: string) {
    if (!search) return '';
    const JSONUrl = JSON.parse('{"' + search.replace("?", "").replace(/&/g, '","').replace(/=/g, '":"') + '"}');
    return _get(JSONUrl, searchKey, '');
}