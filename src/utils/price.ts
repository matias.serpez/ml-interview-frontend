export function formatPriceComma(price: Price) {
    return `
        $ 
        ${price.amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
        ${price.decimals > 0 ? `,${price.decimals}` : ''}
    `;
}