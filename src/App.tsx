// Vendor
import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
// Styles
import './App.css';
// Internal Components
import { AppBar } from './components/AppBar';
// Providers: Theme
import { ThemeProvider } from '@material-ui/styles';
import { theme } from './providers/mui-theme';
import { RootContainer } from './components/RootContainer';
import { ProductsListContainer } from './containers/products-list';
import { ProductDetailsContainer } from './containers/product-details';

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <AppBar />
        <RootContainer>
          <Switch>
            <Route exact path="/items">
              <ProductsListContainer />
            </Route>
            <Route path="/items/:id">
              <ProductDetailsContainer />
            </Route>
          </Switch>
        </RootContainer>
      </Router>
    </ThemeProvider>
  );
}

export default App;
