// VENDOR
import React from 'react';
import { Tooltip } from '@material-ui/core';
// STYLES
import useStyles from './styles';
// ICONS
import WarnIcon from '@material-ui/icons/Warning';

interface LoadingErrorProps {
  error: string;
}

const LoadingError: React.FC<LoadingErrorProps> = ({ error }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Tooltip title={error} placement='top'>
        <div className={classes.errorContainer}>
          <WarnIcon className={classes.errorIcon} />
        </div>
      </Tooltip>
    </div>
  );
};

export default React.memo(LoadingError);
