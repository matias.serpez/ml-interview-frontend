import { Theme } from "@material-ui/core";
import { makeStyles } from '@material-ui/styles';

export default makeStyles((theme: Theme) => ({
    root: {
        position: 'fixed',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    errorContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: `${theme.palette.error.main}44`,
        padding: '10px 20px',
        borderRadius: 3,
    },
    errorIcon: {
        color: `${theme.palette.error.main}!important`,
    }
}));