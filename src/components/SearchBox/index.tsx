// Vendor
import React, { memo, useState } from 'react'
// Styles
import useStyles from './styles'
// Static
import searchIcon from '../../assets/ic_Search.png';
import { useHistory, useLocation } from 'react-router-dom';
// Utils
import { getJsonSearch } from '../../utils/url';

export const SrachBox: React.FC = memo(({ }) => {

    const classes = useStyles();
    const { search } = useLocation();
    const { push } = useHistory();
    const [value, setValue] = useState<string>(getJsonSearch(search, 'search'));

    const onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
        setValue(e.target.value);
    }

    const onSearchHandler = () => {
        push(`/items/?search=${value}`);
    }

    return (
        <div className={classes.root}>
            <input
                className={classes.input}
                placeholder="Nunca dejes de buscar"
                onChange={onChangeHandler}
                value={value}
            />
            <button className={classes.button} onClick={onSearchHandler} >
                <img src={searchIcon} />
            </button>
        </div>
    )
});
