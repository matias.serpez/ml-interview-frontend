import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

export default makeStyles((theme: Theme) => ({
    root: {
        display: 'flex',
    },
    input: {
        border: 'none',
        boxShadow: 'none',
        width: '100%',
        height: theme.spacing(6),
        borderTopLeftRadius: theme.shape.borderRadius,
        borderBottomLeftRadius: theme.shape.borderRadius,
        paddingLeft: theme.spacing(2),
        fontSize: 18,
        '&:focus': {
            outline: 'none',
        }
    },
    button: {
        borderTopRightRadius: theme.shape.borderRadius,
        borderBottomRightRadius: theme.shape.borderRadius,
        border: 'none',
        boxShadow: 'none',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        cursor: 'pointer',
        width: theme.spacing(6),
        '&:focus': {
            outline: '1px solid #fff',
            outlineOffset: -4,
            border: 'none'
        },
    }
}));