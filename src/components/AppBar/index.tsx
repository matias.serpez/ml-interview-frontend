// Vendor
import React, { memo } from 'react'
// Internal components
import { SrachBox } from '../SearchBox';
// Static
import logo from '../../assets/Logo_ML@2x.png';
// Styles
import useStyles from './styles';
// MUI
import { Grid } from '@material-ui/core';

interface AppBarProps {
}

export const AppBar: React.FC<AppBarProps> = memo(({ }) => {

    const classes = useStyles();

    return (
        <Grid container className={classes.root} >
            <Grid item xs={1} >
                <div className={classes.logoContainer}>
                    <img src={logo} />
                </div>
            </Grid>
            <Grid item xs={11}>
                <div>
                    <SrachBox />
                </div>
            </Grid>
        </Grid >
    )
});
