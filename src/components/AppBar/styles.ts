import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

export default makeStyles((theme: Theme) => {
    return {
        root: {
            backgroundColor: theme.palette.primary.main,
            height: 72,
            alignItems: 'center',
            justifyContent: 'center',
            paddingLeft: theme.spacing(24),
            paddingRight: theme.spacing(24),
        },
        logoContainer: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-start',
            '& > img': {
                height: 50,
            }
        }
    }
});