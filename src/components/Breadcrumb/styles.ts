import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";
import { fontWeight } from "@material-ui/system";

export default makeStyles((theme: Theme) => ({
    root: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    icon: {
        color: '#999999',
        marginRight: theme.spacing(1),
    },
    text: {
        fontSize: 14,
        color: '#999999',
        marginRight: theme.spacing(1),
    },
    textLast: {
        fontWeight: 700,
    }
}));