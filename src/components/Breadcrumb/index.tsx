// VENDOR
import React, { memo } from 'react'
// STYLES
import useStyles from './styles';
// MUI
import { Typography } from '@material-ui/core';
import ArrowIcon from '@material-ui/icons/ChevronRight';

interface BreadcrumbProps {
    items: string[]
}

export const Breadcrumb: React.FC<BreadcrumbProps> = memo(({ items }) => {

    const classes = useStyles();

    return (
        <div className={classes.root}>
            {items.map((item, index) => {
                const isLastItem = index === items.length - 1;
                return (
                    <React.Fragment>
                        <Typography className={`${classes.text} ${isLastItem && classes.textLast}`}>A</Typography>
                        {!isLastItem && <ArrowIcon className={classes.icon} />}
                    </React.Fragment>
                )
            })}
        </div>
    )
});
