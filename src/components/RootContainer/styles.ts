import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

export default makeStyles((theme: Theme) => ({
    root: {
        display: 'flex',
        '& > div': {
            display: 'flex',
            flexDirection: 'column',
            flex: 1,
        },
        paddingLeft: theme.spacing(24),
        paddingRight: theme.spacing(24),
    },
}));