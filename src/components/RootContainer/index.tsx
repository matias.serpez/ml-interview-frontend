// VENDOR
import React, { memo } from 'react'
// STYLES
import useStyles from './styles';


export const RootContainer: React.FC = memo(props => {

    const classes = useStyles();

    return (
        <div className={classes.root}>
            {props.children}
        </div>
    )
});
