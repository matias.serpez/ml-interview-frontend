import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

export default makeStyles((theme: Theme) => ({
    imgContainer: {
        padding: theme.spacing(4),
    },
    img: {
        width: '100%',
    },
    condition: {
        marginTop: theme.spacing(4),
        fontSize: 14,
    },
    mainTitle: {
        fontSize: 24,
    },
    price: {
        fontSize: 46,
    },
    buy: {
        marginRight: theme.spacing(4),
    },
    descriptiveContainer: {
        marginLeft: theme.spacing(4),
    },
    title: {
        fontSize: 26,
        marginBottom: theme.spacing(4),
    },
    description: {
        fontSize: 18,
        marginBottom: theme.spacing(4),
    },
}));