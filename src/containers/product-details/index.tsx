// VENDOR
import React, { memo } from 'react'
// STYLES
import useStyles from './styles';
// MUI
import { Grid, Typography, Button } from '@material-ui/core';
import { Breadcrumb } from '../../components/Breadcrumb';
import { Paper } from '@material-ui/core';
// INTERNAL
import { Loader } from './components/loader';
import ErrorMessage from '../../components/ErrorMessage';
import { useFetchApi } from '../../hooks/useFetchApi';
import { formatPriceComma } from '../../utils/price';
import { useParams } from 'react-router';

interface ProductDetailsContainerProps {

}

export const ProductDetailsContainer: React.FC<ProductDetailsContainerProps> = memo(({ }) => {

    const classes = useStyles();
    const { id } = useParams();
    const [{ data, error, loading }] = useFetchApi<ProductResponse>('get', `/${id}`);

    if (error) return <ErrorMessage error={error} />
    if (loading) return <Loader />

    const product = data!.item;
    const price = formatPriceComma(product.price);
    const condition = product.condition === 'new' ? 'Nuevo' : 'Usado';

    return (
        <div>
            <Breadcrumb items={['AAA', 'BBB', 'CCC']} />
            <Paper>
                <Grid container justify="center">
                    <Grid item xs={7}>
                        <div className={classes.imgContainer}>
                            <img className={classes.img} src={product.picture} />
                        </div>
                    </Grid>
                    <Grid item xs={3}>
                        <Typography className={classes.condition}>{condition}</Typography>
                        <Typography className={classes.mainTitle}>{product.title}</Typography>
                        <Typography className={classes.price}>{price}</Typography>
                        <Button
                            size="large"
                            color="secondary"
                            variant="contained"
                            className={classes.buy}
                            fullWidth
                        >
                            Comprar
                    </Button>
                    </Grid>
                    <Grid item xs={10} className={classes.descriptiveContainer}>
                        <Typography className={classes.title}>Descripción del producto</Typography>
                        <Typography className={classes.description}>{product.description}</Typography>
                    </Grid>
                </Grid>
            </Paper>
        </div>

    )
});
