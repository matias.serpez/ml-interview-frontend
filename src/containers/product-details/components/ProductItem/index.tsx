// VENDOR
import React, { memo } from 'react'
// STYLES
import useStyles from './styles';
// MUI
import { Grid, Typography } from '@material-ui/core';


interface ProductListItemProps {
}

export const ProductListItem: React.FC<ProductListItemProps> = memo(({ }) => {

    const classes = useStyles();

    return (
        <Grid container className={classes.root} justify="center">
            <Grid item xs={7}>
                <img className={classes.image} src="https://placeholder.it/100x100" />
            </Grid>
            <Grid item xs={6} justify="center">
                <Typography className={classes.price}>$1.000</Typography>
                <Typography className={classes.title}>La lal a al la sadj ss sa kdkasd asl slj</Typography>
                <Typography className={classes.title}>La lal a al la sadj ss sa kdkasd asl slj</Typography>
                <Typography className={classes.title}>La lal a al la sadj ss sa kdkasd asl slj</Typography>
                <button>Comprar</button>
            </Grid>
            <Grid item xs={2} justify="center">
                <Typography className={classes.location}>Capital Federal</Typography>
            </Grid>
        </Grid>
    )
});
