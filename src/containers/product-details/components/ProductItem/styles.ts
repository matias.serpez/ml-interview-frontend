import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

export default makeStyles((theme: Theme) => ({
    root: {
        marginBottom: theme.spacing(2),
        marginTop: theme.spacing(2),
    },
    image: {
        width: 180,
        height: 180,
        borderRadius: 4,
    },
    price: {
        color: '#333333',
        fontSize: 24,
        marginBottom: theme.spacing(4),
    },
    title: {
        color: '#333333',
        fontSize: 18,
    },
    location: {
        fontSize: 12,
        color: '#999999'
    },
}));