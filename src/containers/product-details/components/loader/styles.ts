import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

export default makeStyles((theme: Theme) => ({
    img: {
        marginTop: theme.spacing(4),
        marginRight: theme.spacing(4),
    },
    detail: {
        marginTop: theme.spacing(4),
    }
}));