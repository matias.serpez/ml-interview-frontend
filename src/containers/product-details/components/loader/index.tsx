// VENDOR
import React, { memo } from 'react'
// STYLES
import useStyles from './styles';
// MUI
import { Grid } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';


interface LoaderProps {
}

export const Loader: React.FC<LoaderProps> = memo(({ }) => {

    const classes = useStyles();

    return (
        <Grid container justify="center">
            <Grid item xs={7}>
                <Skeleton variant="rect" className={classes.img} height={400} />
            </Grid>
            <Grid item xs={3}>
                <Skeleton variant="rect" className={classes.detail} height={14} width={'50%'} />
                <Skeleton variant="rect" className={classes.detail} height={24} width={'70%'} />
                <Skeleton variant="rect" className={classes.detail} height={46} width={'25%'} />
            </Grid>
        </Grid>
    )
});
