// VENDOR
import React, { memo, useEffect } from 'react'
// INTERNAL COMPONENTS
import { ProductListItem } from './components/ProductListItem';
import { Breadcrumb } from '../../components/Breadcrumb';
import { useLocation } from 'react-router';
// MUI
import { Paper } from '@material-ui/core';
// INT
import { useFetchApi } from '../../hooks/useFetchApi';
import ErrorMessage from '../../components/ErrorMessage';
import { Loader } from './components/loader';
import { getJsonSearch } from '../../utils/url';
// CONSTANTS
import { MAX_ITEMS_LIST_SEARCH } from '../../constants';

export const ProductsListContainer: React.FC = memo(({ }) => {

    const { search } = useLocation();
    const searchValue = getJsonSearch(search, 'search');
    const [{ data, error, loading }, refetch] = useFetchApi<ProductsResponse>('get', `?q=${searchValue}`);

    useEffect(() => {
        refetch();
    }, [searchValue])

    if (error) return <ErrorMessage error={error} />
    if (loading) return <Loader />

    const truncatedList = data!.items.slice(0, MAX_ITEMS_LIST_SEARCH);

    // TODO: Implement not results view.

    return (
        <div>
            <Breadcrumb items={['AAA', 'BBB', 'CCC']} />
            <Paper>
                {truncatedList.map((item, index) => {
                    const isLast = index === truncatedList.length - 1;
                    return <ProductListItem key={`list_item_${item.id}`} isLast={isLast} item={item} />
                })}
            </Paper>
        </div>
    )
});
