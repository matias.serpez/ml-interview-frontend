import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

export default makeStyles((theme: Theme) => ({
    img: {
        margin: theme.spacing(4),
    },
    title: {
        marginTop: theme.spacing(8),
        marginBottom: theme.spacing(4),
    },
    detail: {
        marginTop: theme.spacing(2),
    }
}));