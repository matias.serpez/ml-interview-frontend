// VENDOR
import React, { memo } from 'react'
// STYLES
import useStyles from './styles';
// MUI
import { Grid } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';


export const Loader: React.FC = memo(({ }) => {

    const classes = useStyles();

    const getLoaderPattern = () => (
        <Grid container>
            <Grid item xs={3}>
                <Skeleton variant="rect" className={classes.img} height={200} width={200} />
            </Grid>
            <Grid item xs={9}>
                <Skeleton variant="rect" className={classes.title} height={25} width={'20%'} />
                <Skeleton variant="rect" className={classes.detail} height={10} width={'90%'} />
                <Skeleton variant="rect" className={classes.detail} height={10} width={'95%'} />
                <Skeleton variant="rect" className={classes.detail} height={10} width={'100%'} />
                <Skeleton variant="rect" className={classes.detail} height={10} width={'100%'} />
            </Grid>
        </Grid>
    )

    return (
        <div>
            {[1, 2, 3].map(getLoaderPattern)}
        </div>
    )
});
