// VENDOR
import React, { memo } from 'react'
import { Link } from 'react-router-dom';
// STYLES
import useStyles from './styles';
// MUI
import { Grid, Typography } from '@material-ui/core';
// ASSETS
import shipURL from '../../../../assets/ic_shipping@2x.png';
import { formatPriceComma } from '../../../../utils/price';

interface ProductListItemProps {
    isLast?: boolean
    item: Product
}

export const ProductListItem: React.FC<ProductListItemProps> = memo(({ isLast, item }) => {

    const classes = useStyles();

    const formatedPrice = formatPriceComma(item.price);

    return (
        <Link to={`/items/${item.id}`} className={classes.link}>
            <Grid container className={classes.root} justify="center">
                <Grid item xs={2}>
                    <img className={classes.image} src={item.picture} />
                </Grid>
                <Grid item xs={8} justify="center">
                    <div className={classes.priceContainer}>
                        <Typography className={classes.price}>{formatedPrice}</Typography>
                        {item.free_shipping && <img className={classes.ship} height="16" src={shipURL} />}
                    </div>
                    <Typography className={classes.title}>{item.title}</Typography>
                </Grid>
                <Grid item xs={2} justify="center">
                    <Typography className={classes.location}>Capital Federal</Typography>
                </Grid>
                {!isLast && <Grid item xs={12} className={classes.divider} />}
            </Grid>
        </Link>
    )
});
