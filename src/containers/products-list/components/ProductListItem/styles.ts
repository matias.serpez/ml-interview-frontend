import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

export default makeStyles((theme: Theme) => ({
    link: {
        textDecoration: 'none',
    },
    root: {
        marginBottom: theme.spacing(2),
        marginTop: theme.spacing(2),
        cursor: 'pointer',
    },
    image: {
        width: 180,
        height: 180,
        borderRadius: 4,
        padding: theme.spacing(2),
    },
    priceContainer: {
        marginTop: theme.spacing(4),
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    price: {
        color: '#333333',
        fontSize: 24,
    },
    ship: {
        marginLeft: theme.spacing(2),
    },
    title: {
        marginTop: theme.spacing(4),
        color: '#333333',
        fontSize: 18,
    },
    location: {
        marginTop: theme.spacing(8),
        fontSize: 12,
        color: '#999999'
    },
    divider: {
        borderBottom: '1px solid #EEEEEE',
        marginRight: theme.spacing(1),
        marginLeft: theme.spacing(1),
    }
}));