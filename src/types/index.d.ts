declare interface Author {
  name: string;
  lastname: string;
}

declare interface Price {
  currency: string;
  amount: number;
  decimals: number;
}

declare interface Author {
  name: string;
  lastname: string;
}

declare interface Product {
  id: string;
  title: string;
  price: Price;
  picture: string;
  condition: string;
  free_shipping: boolean;
  sold_quantity?: string;
  description?: string;
}

declare interface ProductsResponse {
  author: Author;
  categories: string[];
  items: Product[];
}

declare interface ProductResponse {
  author: Author;
  item: Product;
}
