import { useState, useEffect } from 'react';
import { mlProductsService } from '../services/ml-products';
import { AxiosError } from 'axios';

type FetchResponse<T> = {
    loading: boolean
    error: string | null
    data: T | null
}

export function useFetchApi<T>(method: 'put' | 'get', url?: string): [FetchResponse<T>, () => void] {
    const [loading, setLoading] = useState<boolean>(true);
    const [data, setData] = useState<T | null>(null);
    const [error, setError] = useState<string | null>(null);


    const fetch = () => {
        setLoading(true);
        setError(null);
        mlProductsService({ method, url })
            .then(({ data }: { data: T }) => {
                setData(data);
            })
            .catch((error: AxiosError) => {
                setError(error.message);
            })
            .finally(() => {
                setLoading(false);
            });
    }

    useEffect(() => {
        fetch();
    }, []);

    return [{ loading, data, error }, fetch];
}