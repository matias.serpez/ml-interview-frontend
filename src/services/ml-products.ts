// VENDOR
import axios from 'axios';
// CONSTANTS
import { ML_API_PRODUCTS_URL } from '../constants';

export const mlProductsService = axios.create({
    baseURL: ML_API_PRODUCTS_URL,
});