# Entrevista MercadoLibre.
Proyecto `Frontend` para entrevista técnica de Mercado Libre.

El lenguaje utilizado fue JS, utilizando la librería de código ReactJS.

# Iniciar el proyecto (local)
1. Ingresar en una colsola o terminal al root del proyecto.
1. Ejecutar el comando `yarn` ó `npm install`.
1. Ejecutar el comando `npm start`.

# Test: TODO.

# Deploy: TODO.

# Contacto
matias.serpez@gmail.com